const i = 1,
    o = 0,
    x = 2
let d
let board = 'xxx---ooo'
const player = ['-', 'o', 'x']
const cols = ['', '#11406d', 'rgb(19, 76, 65)']
const AI = 2

const arrayfy = x => [parseInt((x - 1) / 3), (x - 1) % 3]
const isValidMove = (in1, in2) => {
    const ar1 = arrayfy(in1),
        ar2 = arrayfy(in2)
    return in1 != in2 && (in1 == 5 || in2 == 5 || Math.pow(ar1[0] - ar2[0], 2) + Math.pow(ar1[1] - ar2[1], 2) == 1)

}

const showBoard = b => console.log(b.split('').map((x, i) => (i % 3 ? ' ' : '\n   ') + x).join('') + '\n')
const boardAfterMove = (board, from, to) => board.split('').map((x, i, a) => i == from - 1 ? a[to - 1] : (i == to - 1 ? a[from - 1] : x)).join('')
const gamePlan = (board = 'xxx---ooo', p = 2) => {
    let myStands = findStands(board, p)
    let emptyStands = findStands(board, 0)
    let myValidMoves = []
    emptyStands.map(x => {
        [0, 1, 2].map(i => {
            if (isValidMove(x, myStands[i])) myValidMoves.push(moveAttributes(board, myStands[i], x))
        })
    })
    return myValidMoves
}

const moveAttributes = (board, from, to) => {
    let boardAfterMoving = boardAfterMove(board, from, to)
    let p = player.indexOf(board.charAt(from - 1))
    let nextStand = findStands(boardAfterMoving, p)

    return {
        from,
        to,
        boardAfterMoving,
        willWin: [0].concat(nextStand).reduce((x, i) => x + magicSquare[i - 1]) == 15 && (['789', '123'][p - 1] != nextStand.sort().join('')),
        nextOpponentsMoves: p == AI ? gamePlan(boardAfterMoving, 3 - p) : null
    }
}

const findStands = (board, p) => {
    let stands = []
    board.split('').map((x, i) => {
        if (x == player[p]) stands.push(i + 1)
    })
    return stands
}

const magicSquare = [2, 7, 6, 9, 5, 1, 4, 3, 8]

const hasPlayerWon = (board, p) => [0].concat(findStands(board, p)).reduce((x, i) => x + magicSquare[i - 1]) == 15 && (['789', '123'][p - 1] != findStands(board, p).sort().join(''))



board.split('').map((x, i) => document.getElementById('d' + (i + 1)).addEventListener('click', () => doStep(i + 1)))

let pressed = 0
const doStep = x => {
    if (pressed != 0) {
        if (isValidMove(pressed, x) && findStands(board, 0).indexOf(x) > -1) {
            moveIt(pressed, x, true)
        } else {
            paint()
            showNotif("Invalid Moves!")
        }
        pressed = 0

    } else {
        if (findStands(board, 1).indexOf(x) > -1) {
            pressed = x
            document.getElementById('d' + x).style.background = 'indigo'
        } else {
            paint()
            showNotif("Wrong Bead!")
        }
    }

}

const chooseRandom = x => x[parseInt(x.length * Math.random())]

const moveIt = (x, y, isHuman = false) => {
    board = boardAfterMove(board, x, y)
    paint()
    if (isHuman) {
        nowPlan = gamePlan(board)

        mustWinMoves = []
        mustLoseMoves = []

        mustWinNextMoves = []
        mustLoseNextMoves = []

        mayWinNextMoves = []

        okNowMoves = []
        okNowAndNextMoves = []


        nowPlan.map((x, i) => {

            mustWin = x.willWin
            mustLose = false

            mustWinNext = true
            mayWinNext = false

            mustLoseNext = false

            x.nextOpponentsMoves.map(y => {
                mustLose = mustLose || y.willWin
                winThisTime = false
                mustLoseHere = true
                gamePlan(y.boardAfterMoving).map(z => {
                    winThisTime = winThisTime || z.willWin
                    loseThisTime = false
                    z.nextOpponentsMoves.map(v => {
                        loseThisTime = loseThisTime || v.willWin
                        console.log(100)
                    })
                    mustLoseHere = mustLoseHere && loseThisTime
                })

                mustWinNext = mustWinNext && winThisTime
                mustLoseNext = mustLoseNext || mustLoseHere

                mayWinNext = mayWinNext || winThisTime
            })

            if (mustWin) mustWinMoves.push(i)
            if (mustLose) mustLoseMoves.push(i)
            else okNowMoves.push(i)

            if (mustWinNext && !mustLose) mustWinNextMoves.push(i)
            if (mustLoseNext) mustLoseNextMoves.push(i)
            else if (!mustLose && !([0].concat(findStands(board, AI)).reduce((t, m) => t + magicSquare[m - 1]) == 15 && parseInt(x.to) == 5)) {
                okNowAndNextMoves.push(i)
                if (mayWinNext) mayWinNextMoves.push(i)
            }



        })

        let myMove = nowPlan[chooseBest(mustWinMoves, mustWinNextMoves, mayWinNextMoves, okNowAndNextMoves, okNowMoves, nowPlan.map((x, i) => i))]


        if (myMove)
            moveIt(myMove.from, myMove.to, false)
        else console.error('Trapped')

    }
}


const chooseBest = (...b) => {
    for (a of b)
        if (a.length)
            return chooseRandom(a)
}

const showNotif = x => {
    document.getElementById('result').style.visibility = 'visible'
    document.getElementById('result').innerHTML = x
}

const markWinner = x => findStands(board, x).map(y => document.getElementById('d' + y).style.borderColor = 'white')

const paint = () => {
    document.getElementById('result').style.visibility = 'hidden'
    document.getElementById('again').style.visibility = 'hidden'
    board.split('').map((x, i) => {
        document.getElementById('d' + (i + 1)).style.background = cols[player.indexOf(x)]
        document.getElementById('d' + (i + 1)).style.borderColor = '#416bb9'
    })
    if (hasPlayerWon(board, 2)) {
        showNotif("AI wins!")
        markWinner(2)
        document.getElementById('again').style.visibility = 'visible'
    }
    if (hasPlayerWon(board, 1)) {
        showNotif("You Win!")
        markWinner(1)
        document.getElementById('again').style.visibility = 'visible'
    }
}

paint()